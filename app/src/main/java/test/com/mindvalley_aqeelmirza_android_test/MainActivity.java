package test.com.mindvalley_aqeelmirza_android_test;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.OvershootInterpolator;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import test.com.mindvalley_aqeelmirza_android_test.Adapters.RecyclerViewAdapter;
import test.com.mindvalley_aqeelmirza_android_test.Animators.ScaleInAnimationAdapter;
import test.com.mindvalley_aqeelmirza_android_test.Models.DataModel;
import test.com.mindvalley_aqeelmirza_android_test.Models.FullDetailsModel;
import test.com.mindvalley_aqeelmirza_android_test.Models.ProfileImage;
import test.com.mindvalley_aqeelmirza_android_test.Models.User;
import test.com.mindvalley_aqeelmirza_android_test.Utility.MindValley;
import test.com.mindvalley_aqeelmirza_android_test.Utility.RecyclerItemClickListener;


public class MainActivity extends AppCompatActivity {
    SwipeRefreshLayout refreshLayout;
    RecyclerView recyclerView;
    GridLayoutManager gridLayoutManager;
    LinearLayoutManager linearLayoutManager;
    DataModel dataModel;
    User user;
    ProfileImage profileimages;
    ArrayList<DataModel> dataModelArrayList;
    ArrayList<User> userArrayList;
    ArrayList<ProfileImage> profileImageArrayList;
    RecyclerViewAdapter recyclerViewAdapter;
    FloatingActionButton fab;
    int type = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        refreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        gridLayoutManager = new GridLayoutManager(this, 2);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(gridLayoutManager);

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_format_list_bulleted_white_24dp));

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (type == 1) {
                    toListView();
                } else {
                    toGridView();
                }
            }
        });

        getDataService();

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshItems();

            }
        });

        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this,
                new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, final int position) {
                        final String username = dataModelArrayList.get(position).getUser().getUsername();
                        final Snackbar snackbar = Snackbar.make(view, "UserName - " + username, Snackbar.LENGTH_SHORT);
                        snackbar.setAction("View Details", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                FullDetailsModel fullDetailsModel = new FullDetailsModel();
                                fullDetailsModel.setCreated_at(dataModelArrayList.get(position).getCreatedAt());
                                fullDetailsModel.setHeight(dataModelArrayList.get(position).getHeight());
                                fullDetailsModel.setLiked_by_user(dataModelArrayList.get(position).getLikedByUser());
                                fullDetailsModel.setLikes(dataModelArrayList.get(position).getLikes());
                                fullDetailsModel.setName(dataModelArrayList.get(position).getUser().getName());
                                fullDetailsModel.setUsername(dataModelArrayList.get(position).getUser().getUsername());
                                fullDetailsModel.setProfile_image(dataModelArrayList.get(position).getUser().getProfileImage().getLarge());
                                fullDetailsModel.setWidth(dataModelArrayList.get(position).getWidth());

                                Intent in = new Intent(MainActivity.this, FullDetailsActivity.class);
                                in.putExtra("fulldetails", fullDetailsModel);
                                startActivity(in);
                            }
                        }).show();
                    }
                }));
    }

    void refreshItems() {
        // Load items
        getDataService();

        // Load complete
        onItemsLoadComplete();
    }

    void onItemsLoadComplete() {
        // Update the adapter and notify data set changed
        recyclerViewAdapter.notifyDataSetChanged();

        // Stop refresh animation
        refreshLayout.setRefreshing(false);
    }

    public void getDataService() {

        String RequestUrl = "http://pastebin.com/raw/wgkJgazE";

        StringRequest requestobj = new StringRequest(Request.Method.POST,
                RequestUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("Response", response);
                        try {
                            JSONArray array = new JSONArray(response.toString());
                            dataModelArrayList = new ArrayList<>();
                            userArrayList = new ArrayList<>();
                            profileImageArrayList = new ArrayList<>();
                            for (int i = 0; i < array.length(); i++) {
                                dataModel = new DataModel();
                                user = new User();
                                profileimages = new ProfileImage();
                                JSONObject dataObject = array.getJSONObject(i);
                                dataModel.setId(dataObject.getString("id"));
                                dataModel.setCreatedAt(dataObject.getString("created_at"));
                                dataModel.setWidth(dataObject.getInt("width"));
                                dataModel.setHeight(dataObject.getInt("height"));
                                dataModel.setLikes(dataObject.getInt("likes"));
                                dataModel.setLikedByUser(dataObject.getBoolean("liked_by_user"));
                                dataModel.setColor(dataObject.getString("color"));

                                JSONObject userObject = dataObject.getJSONObject("user");

                                user.setId(userObject.getString("id"));
                                user.setUsername(userObject.getString("username"));
                                user.setName(userObject.getString("name"));

                                JSONObject profileimagesobj = userObject.getJSONObject("profile_image");

                                profileimages.setSmall(profileimagesobj.getString("small"));
                                profileimages.setMedium(profileimagesobj.getString("medium"));
                                profileimages.setLarge(profileimagesobj.getString("large"));

                                user.setProfileImage(profileimages);
                                dataModel.setUser(user);


                                profileImageArrayList.add(profileimages);
                                userArrayList.add(user);
                                dataModelArrayList.add(dataModel);
                            }
                            recyclerViewAdapter = new RecyclerViewAdapter(MainActivity.this, R.layout.items_recyclerview, dataModelArrayList);
                            ScaleInAnimationAdapter alphaAdapter = new ScaleInAnimationAdapter(recyclerViewAdapter);
                            alphaAdapter.setFirstOnly(true);
                            alphaAdapter.setDuration(500);
                            alphaAdapter.setInterpolator(new OvershootInterpolator(.5f));

                            recyclerView.setAdapter(alphaAdapter);


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("ERROR", "Error: " + error.getMessage());
                System.out.println(error.getStackTrace());
                Toast.makeText(MainActivity.this, "Please try again", Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                try {
                    Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
                    if (cacheEntry == null) {
                        cacheEntry = new Cache.Entry();
                    }
                    final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
                    final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
                    long now = System.currentTimeMillis();
                    final long softExpire = now + cacheHitButRefreshed;
                    final long ttl = now + cacheExpired;
                    cacheEntry.data = response.data;
                    cacheEntry.softTtl = softExpire;
                    cacheEntry.ttl = ttl;
                    String headerValue;
                    headerValue = response.headers.get("Date");
                    if (headerValue != null) {
                        cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
                    }
                    headerValue = response.headers.get("Last-Modified");
                    if (headerValue != null) {
                        cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
                    }
                    cacheEntry.responseHeaders = response.headers;
                    final String jsonString = new String(response.data,
                            HttpHeaderParser.parseCharset(response.headers));
                    return Response.success(jsonString, cacheEntry);
                } catch (UnsupportedEncodingException e) {
                    return Response.error(new ParseError(e));
                }
            }

            @Override
            protected void deliverResponse(String response) {
                super.deliverResponse(response);
            }

            @Override
            public void deliverError(VolleyError error) {
                super.deliverError(error);
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                return super.parseNetworkError(volleyError);
            }

        };
        requestobj.setRetryPolicy(new DefaultRetryPolicy(2 * DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        );
        MindValley.getInstance().addToRequestQueue(requestobj);


    }

    void toListView() {
        type = 2;
        fab.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.ic_grid_on_white_24dp));
        gridLayoutManager = new GridLayoutManager(MainActivity.this, 1);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(gridLayoutManager);
    }

    void toGridView() {
        type = 1;
        gridLayoutManager = new GridLayoutManager(MainActivity.this, 2);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(gridLayoutManager);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.ic_format_list_bulleted_white_24dp));
    }
}

