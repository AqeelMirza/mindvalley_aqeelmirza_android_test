package test.com.mindvalley_aqeelmirza_android_test.Models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Aqeel.mirza on 8/27/2016.
 */
public class FullDetailsModel implements Parcelable {
    String created_at;
    int height;
    int width;
    int likes;
    Boolean liked_by_user;
    String username;
    String profile_image;
    String name;

    public FullDetailsModel(Parcel in) {
        created_at = in.readString();
        height = in.readInt();
        width = in.readInt();
        likes = in.readInt();
        username = in.readString();
        profile_image = in.readString();
        name = in.readString();
        liked_by_user = in.readByte() != 0;
    }

    public static final Creator<FullDetailsModel> CREATOR = new Creator<FullDetailsModel>() {
        @Override
        public FullDetailsModel createFromParcel(Parcel in) {
            return new FullDetailsModel(in);
        }

        @Override
        public FullDetailsModel[] newArray(int size) {
            return new FullDetailsModel[size];
        }
    };

    public FullDetailsModel() {

    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }



    public Boolean getLiked_by_user() {
        return liked_by_user;
    }

    public void setLiked_by_user(Boolean liked_by_user) {
        this.liked_by_user = liked_by_user;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(created_at);

        dest.writeInt(height);
        dest.writeInt(width);
        dest.writeInt(likes);
        dest.writeString(username);
        dest.writeString(profile_image);
        dest.writeString(name);
        dest.writeByte((byte) (liked_by_user ? 1 : 0));
    }
}
