package test.com.mindvalley_aqeelmirza_android_test;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import test.com.mindvalley_aqeelmirza_android_test.Models.FullDetailsModel;
import test.com.mindvalley_aqeelmirza_android_test.Utility.VolleySingleton;

/**
 * Created by Aqeel.mirza on 8/27/2016.
 */
public class FullDetailsActivity extends Activity {
    FullDetailsModel fullDetailsModel;
    ImageView image_like;
    TextView profile_name, likes, createdat, username;
    NetworkImageView profile_image;
    ImageLoader imageLoader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fulldetails);

        imageLoader = VolleySingleton.getInstance().getImageLoader();

        fullDetailsModel = getIntent().getExtras().getParcelable("fulldetails");

        profile_image = (NetworkImageView) findViewById(R.id.profile_image);
        profile_name = (TextView) findViewById(R.id.profile_name);
        image_like = (ImageView) findViewById(R.id.image_like);
        likes = (TextView) findViewById(R.id.tv_likes);
        createdat = (TextView) findViewById(R.id.tv_createdat);
        username = (TextView) findViewById(R.id.tv_username);

        String url = fullDetailsModel.getProfile_image();

        imageLoader.get(url, ImageLoader.getImageListener(profile_image,
                R.color.white, android.R.drawable
                        .ic_dialog_alert));
        profile_image.setImageUrl(url, imageLoader);

        profile_name.setText(fullDetailsModel.getName());
        likes.setText(fullDetailsModel.getLikes() + " Likes");

        final Boolean isUserLiked = fullDetailsModel.getLiked_by_user();

        if (isUserLiked) {
            image_like.setImageResource(R.drawable.liked);
        } else {
            image_like.setImageResource(R.drawable.like);
        }

        username.setText(fullDetailsModel.getUsername());

        createdat.setText(fullDetailsModel.getCreated_at());

    }

   /* @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }*/

}
