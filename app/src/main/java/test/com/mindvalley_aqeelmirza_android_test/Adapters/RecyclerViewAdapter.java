package test.com.mindvalley_aqeelmirza_android_test.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import java.util.ArrayList;

import test.com.mindvalley_aqeelmirza_android_test.Models.DataModel;
import test.com.mindvalley_aqeelmirza_android_test.R;
import test.com.mindvalley_aqeelmirza_android_test.Utility.VolleySingleton;

/**
 * Created by Aqeel.mirza on 8/27/2016.
 */
public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.RecyclerViewHolders> {
    public ArrayList<DataModel> itemList;
    public Context context;
    int layoutResourceId;
    ImageLoader imageLoader;
    Boolean isLiked;

    public RecyclerViewAdapter(Context context, int layoutResourceId, ArrayList<DataModel> dataModelArrayList) {

        this.context = context;
        this.layoutResourceId = layoutResourceId;
        this.itemList = dataModelArrayList;

        imageLoader = VolleySingleton.getInstance().getImageLoader();
    }

    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(layoutResourceId, null);
        RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);


        return rcv;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolders holder, final int position) {

        String url = itemList.get(position).getUser().getProfileImage().getLarge();

        imageLoader.get(url, ImageLoader.getImageListener(holder.profile_image,
                R.color.white, android.R.drawable
                        .ic_dialog_alert));
        holder.profile_image.setImageUrl(url, imageLoader);

        holder.profile_name.setText(itemList.get(position).getUser().getName());
        holder.likes.setText(itemList.get(position).getLikes() + " Likes");

        final Boolean isUserLiked = itemList.get(position).getLikedByUser();

        if (isUserLiked) {
            isLiked = true;
            holder.image_like.setImageResource(R.drawable.liked);
        } else {
            isLiked = false;
            holder.image_like.setImageResource(R.drawable.like);
        }
        final int likesval = itemList.get(position).getLikes();
        holder.image_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isLiked) {
                    isLiked = false;
                    holder.image_like.setImageResource(R.drawable.like);
                    holder.likes.setText(likesval + " Likes");
                } else {
                    isLiked = true;
                    holder.image_like.setImageResource(R.drawable.liked);
                    holder.likes.setText(likesval + 1 + " Likes");
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return itemList == null ? 0 : itemList.size();
    }

    public static class RecyclerViewHolders extends RecyclerView.ViewHolder {

        ImageView image_like;
        TextView profile_name, likes;
        CardView cardView;
        NetworkImageView profile_image;

        public RecyclerViewHolders(View itemView) {
            super(itemView);
            profile_image = (NetworkImageView) itemView.findViewById(R.id.profile_image);
            profile_name = (TextView) itemView.findViewById(R.id.profile_name);
            image_like = (ImageView) itemView.findViewById(R.id.image_like);
            likes = (TextView) itemView.findViewById(R.id.tv_likes);
            cardView = (CardView) itemView.findViewById(R.id.card_view);

        }
    }

}
